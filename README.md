# Script and data associated with: Composition and pair similarity of the cloacal microbiome in the polyandrous red phalarope (*Phalaropus fulicarius*)

## Description of files

### Files for QIIME2

- Script for QIIME2: QIIME2script.Rmd

### Files for picrust2 (MetaCyc dataset)

- Script used to run picrust2: picrust2.Rmd
- Raw fasta file: Decomtamsequences.fasta
- Input biom file: Decomtaminated_biom_2019.biom

### Files for R

- R script: red_phalarope_microbiome.Rmd
- R script htlm: index.html and can be viewed at https://gillinghamlab.gitlab.io/mpi/red-phalarope-microbiome/
- The metadata: MetaDataRedPhalaropeMicrobiome.txt
- ASV biom file: REPHTaxonomy.biom
- MetaCyc biom file: path_abun_unstrat_descrip_round.biom
- 16S phylogenetic tree: treeALLrootedwithNeg.tree (imported to R but not used in publication)
